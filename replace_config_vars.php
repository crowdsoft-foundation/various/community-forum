<?php

$config_file = "/var/install-config.yml";
fileReplaceContent($config_file, "{dbhost}", getenv("DB_HOST"));
fileReplaceContent($config_file, "{dbport}", getenv("DB_PORT"));
fileReplaceContent($config_file, "{dbuser}", getenv("DB_USER"));
fileReplaceContent($config_file, "{dbpassword}", getenv("DB_PASSWORD"));
fileReplaceContent($config_file, "{dbname}", getenv("DB_NAME"));
fileReplaceContent($config_file, "{dbms}", getenv("DB_DRIVER"));


function fileReplaceContent($path, $oldContent, $newContent)
{
    $str = file_get_contents($path);
    $str = str_replace($oldContent, $newContent, $str);
    file_put_contents($path, $str);
}

