FROM php:7.4-apache

RUN apt-get -y update && apt-get install -y \
        libicu-dev \
        zlib1g \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        libzip-dev \
        libonig-dev
RUN a2enmod rewrite && service apache2 restart
RUN docker-php-ext-configure gd --with-freetype --with-jpeg
RUN docker-php-ext-install mysqli pdo pdo_mysql intl gd mbstring && docker-php-ext-enable pdo_mysql

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

COPY ./www /var/www/html
RUN chmod 755 /var/www -R
RUN chmod 777 /var/www/html/cache -R
RUN chmod 777 /var/www/html/store -R
RUN chmod 777 /var/www/html/files -R
RUN chmod 777 /var/www/html/images/avatars/upload -R

COPY ./replace_config_vars.php /var/replace_config_vars.php
COPY ./install-config.yml /var/install-config.yml
RUN chmod 777 /var/install-config.yml
COPY ./run.sh /var/run.sh

RUN chmod 777 /var/run.sh
RUN chmod +x /var/run.sh

RUN mkdir -p /tmp/phpbb/cache
ENV PHPBB____core__cache_dir=/tmp/phpbb/cache

CMD ["/var/run.sh"]

