#!/usr/bin/env sh
set -e
  echo "Start install..."

if [ -s "/var/www/html/config.php" ];then
  echo "Config file found, skipping install"
  if [ -s "/var/www/html/install/app.php" ];then
    mv /var/www/html/install /var/www/html/not_install_anymore || true
  fi
  echo "Starting the server..."
  apache2-foreground
else
  mv /var/www/html/not_install_anymore /var/www/html/install || true
  if [ -s "/var/www/html/install/phpbbcli.php" ];then
    echo "Start install..."
    php /var/replace_config_vars.php
    php /var/www/html/install/phpbbcli.php install /var/install-config.yml
  fi
fi


